# LLEAP_Simulations

This repository is for the ROS-based Exo-skeleton simulations developed by Cal Poly's LLEAP. 

------------------------------------------------------------------------------------------------------------------------------
Installing the hip_knee_v1 package:

PREREQUISITES: 
- ROS Noetic
- ros_control 
- Gazebo Sim
- A catkin workspace

1) Open your catkin workspace, and launch a terminal in your src folder.

2) To download the hip_knee_v1 package, type the following into the terminal: 

    > git clone git@gitlab.com:jonathan.ilagan2019/Prelim_Sim.git

3) Leave the src folder to go back into your catkin workspace. To fully install the package, type the following into the terminal:

    > catkin_make

    hip_knee_v1 should now be fully installed.

    You can control the assembly through the terminal or with rqt.



------------------------------------------------------------------------------------------------------------------------------

Controlling hip_knee_v1 with the terminal

1) Open a terminal inside your catkin workspace and type 

    > source devel/setup.bash

2) In the same terminal, type 

    > roslaunch hip_knee_v1 gazebo.launch

    The model should now be visible in gazebo.

3) Open a new terminal. 

    To control the hip, type

        > rostopic pub -1 /hip_knee_v1/hip_revolute_joint_position_controller/command std_msgs/Float64 <INSERT_NUM_HERE>

    Replace <INSERT_NUM_HERE> with a number between 0 - 2.5, representing positional info in radians

    To control the knee, type

        > rostopic pub -1 /hip_knee_v1/knee_revolute_joint_position_controller/command std_msgs/Float64 <INSERT_NUM_HERE>

    Replace <INSERT_NUM_HERE> with a number between 0 - 1.308, representing positional info in radians



------------------------------------------------------------------------------------------------------------------------------

Controlling hip_knee_v1 with rqt

1) Open a terminal inside your catkin workspace and type 

    > source devel/setup.bash

2) In the same terminal, type 

    > roslaunch hip_knee_v1 gazebo.launch

    The model should now be visible in gazebo.
    
3) Open a new terminal. Type

    > rqt

    rqt should launch. 

4) Under the Plugins dropdown menu, click on Topics>Message Publisher

5) Select the hip_knee_v1/hip_revolute_joint_position_controller/command and click on the green plus button on the right. Repeat for /hip_knee_v1/knee_revolute_joint_position_controller/command.

6) Click on dark arrow on the left of the newly added topics, and you will see a data row. In the expression box, you can enter positional data in radians, which will be sent to the Gazebo simulation.

7) To see the hip oscillate, in the expression box, type 

    > sin(i/100) * 1.25 + 1.25

    To see the knee oscillate, type

    > sin(i/100) * 0.654 + 0.654

    Explanation: i is the time step variable in rqt, and the constants are chosen so that each joint oscillates from their max to min positions.

